package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Properties extends Composite {

	private static PropertiesUiBinder uiBinder = GWT
			.create(PropertiesUiBinder.class);

	interface PropertiesUiBinder extends UiBinder<Widget, Properties> {
	}
	
	@UiField
	SimplePanel mapleA;
	@UiField
	SimplePanel mapleB;	
	@UiField
	SimplePanel renaissance;
	@UiField
	SimplePanel belmont;
	
	public Properties() {
		initWidget(uiBinder.createAndBindUi(this));
		mapleA.add(new Image(DisplayHelper.menuBundle.maple1()));
		mapleB.add(new Image(DisplayHelper.menuBundle.maple2()));
		renaissance.add(new Image(DisplayHelper.menuBundle.renaissance()));
		belmont.add(new Image(DisplayHelper.menuBundle.belmont()));
		
	}

}
