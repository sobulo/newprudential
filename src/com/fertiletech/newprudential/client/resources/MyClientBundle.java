package com.fertiletech.newprudential.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ClientBundle.Source;


public interface MyClientBundle extends ClientBundle {

	//home page logo and slide controls
	ImageResource logo();
	ImageResource back();
	ImageResource forward();
	ImageResource play();
	ImageResource pause();
	ImageResource logoimage();
	 
    //data sources - these are simply treated as urls, i.e. gwt doesn't attempt to preload/create image
    @Source("large/hand.jpg")
    DataResource one();    
	
	@Source("large/core.jpg")
    DataResource two();   
    
    @Source("large/joy.jpg")
    DataResource three(); 	

    @Source("large/bank.jpg")
    DataResource four();    
	
	@Source("large/join.jpg")
    DataResource five();   
    
    @Source("large/couple.jpg")
    DataResource six();
    
    @Source("large/sage.jpg")
    DataResource seven();    

    @Source("large/construct.jpg")
    DataResource eight();
    
    //css file
    @Source("prudential.css")
    PrudentialCssResource prudential();
}

