package com.fertiletech.newprudential.client.resources;

import com.google.gwt.resources.client.CssResource;

public interface PrudentialCssResource extends CssResource
{
	//widget styling
	String mainHeader();
	String playerButton();
	String mainLogo();
	
	//background slideshows
	String bodySlide1();
	String bodySlide2();
	String bodySlide3();
	String bodySlide4();
	String bodySlide5();
	String bodySlide6();
	String bodySlide7();
	String bodySlide8();
	String simpleAnchor();
	String playerControls();
	String popupStyle();
	String popupButton();
	String bottomLogo();
	String backgroundSizer();
}