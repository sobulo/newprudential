package com.fertiletech.newprudential.client.resources.menu;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface MenuBundle extends ClientBundle{
	ImageResource affil();
	ImageResource careers();
	ImageResource choosehome();
	ImageResource contact();
	ImageResource deposit();
	ImageResource director();	
	ImageResource download();
	ImageResource eligible();
	ImageResource history();
	ImageResource lending();
	ImageResource logoimage();
	ImageResource manage();
	ImageResource mortgage();
	ImageResource services();	
	ImageResource renaissance();
	ImageResource maple1();
	ImageResource maple2();
	ImageResource belmont();
}
