package com.fertiletech.newprudential.client;

import com.fertiletech.newprudential.client.resources.MyClientBundle;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class NewPrudential implements EntryPoint {
	DockLayoutPanel headerPanel;
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static MyClientBundle primaryBundle = GWT.create(MyClientBundle.class);

	
	private Timer playThread;
	private TabLayoutPanel backgroundSlider;
	private PopupPanel hoverDisplay;
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		primaryBundle.prudential().ensureInjected();
		hoverDisplay = new PopupPanel();
		optionTwo();
	}

	private void setupHeaderPanel()
	{
		//setup header children
		final Image logo = new Image(primaryBundle.logo());
		final Image play = new Image(primaryBundle.play());
		final Image pause = new Image(primaryBundle.pause());
		final Image forward = new Image(primaryBundle.forward());
		final Image backward = new Image(primaryBundle.back());
		final FocusPanel playPause = new FocusPanel(pause);
		backward.setStyleName(primaryBundle.prudential().playerButton());
		forward.setStyleName(primaryBundle.prudential().playerButton());
		playPause.setStyleName(primaryBundle.prudential().playerButton());		
		logo.setStyleName(primaryBundle.prudential().mainLogo());
		logo.setTitle("New Prudential Mortgage Bank Limited. You can click on me."); //for search engines

		//setup the header panel
		headerPanel = new DockLayoutPanel(Unit.PX);
		final FlowPanel playerControls = new FlowPanel();
		headerPanel.addStyleName(primaryBundle.prudential().mainHeader());
		playerControls.addStyleName(primaryBundle.prudential().playerControls());
		playerControls.add(backward);
		playerControls.add(playPause);
		playerControls.add(forward);
		
		headerPanel.addNorth(logo, 120);
		final Widget topMenu = getMenuItems();
		headerPanel.addNorth(playerControls, 45);
		headerPanel.addNorth(topMenu, 300);
		//headerPanel.addWest(backward, 30);
		//headerPanel.addWest(playPause, 30);
		//headerPanel.addWest(forward, 30);		

		headerPanel.setWidgetSize(playerControls, 0);
		headerPanel.setWidgetSize(topMenu, 0);
		headerPanel.forceLayout();
		headerPanel.setWidgetSize(playerControls, 45);
		headerPanel.setWidgetSize(topMenu, 300);
		headerPanel.animate(2000);
		
		
		//styling
		
		
		//setup click handlers
		playPause.addClickHandler(new ClickHandler() {
			Image currentlyShowing = pause;
			@Override
			public void onClick(ClickEvent event) {
				if(currentlyShowing == play)
				{
					play.removeFromParent();
					playPause.add(pause);
					currentlyShowing = pause;
					playSlide();
				}
				else
				{
					pause.removeFromParent();
					playPause.add(play);
					currentlyShowing = play;
					pauseSlide();
				}
			}
		});
		
		forward.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				moveSlide(true);	
			}
		});
		
		backward.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				moveSlide(false);
			}
		});
				
		logo.addClickHandler(new ClickHandler() {
			final String[] toolTip = {"Click to hide New Prudential website menu", "Click to show New Prudential website menu"};
			boolean visible = true;
			@Override
			public void onClick(ClickEvent event) {
				setWidgetLayout();
				headerPanel.forceLayout();
				visible = !visible;
				logo.setTitle(toolTip[visible?0:1]);
				setWidgetLayout();				
				headerPanel.animate(500);
			}
			
			private void setWidgetLayout()
			{
				headerPanel.setWidgetSize(playerControls, visible?45:0);
				headerPanel.setWidgetSize(topMenu, visible?300:0);
				//headerPanel.setWidgetSize(backward, visible?30:0);
				//headerPanel.setWidgetSize(forward, visible?30:0);
			}
		});
	}
	
	private LayoutPanel getSlidePanel(String styleName)
	{
		LayoutPanel slidePanel = new LayoutPanel();
		slidePanel.addStyleName(styleName);
		slidePanel.addStyleName(primaryBundle.prudential().backgroundSizer());
		return slidePanel;
	}
	
	private void optionTwo()
	{
		backgroundSlider = new TabLayoutPanel(0, Unit.PX);
		backgroundSlider.setWidth("100%");
		backgroundSlider.setHeight("100%");
		backgroundSlider.setAnimationDuration(1500);
		backgroundSlider.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				//hoverDisplay.hide();
			}
		});
		
		final String[] slideStyles = {primaryBundle.prudential().bodySlide1(), primaryBundle.prudential().bodySlide3(), 
				primaryBundle.prudential().bodySlide4(), primaryBundle.prudential().bodySlide6(),
				primaryBundle.prudential().bodySlide7(), primaryBundle.prudential().bodySlide8()};

		for(int i=0; i < slideStyles.length; i++)
			backgroundSlider.add(getSlidePanel(slideStyles[i]));
				
		int initialSlide = Random.nextInt(backgroundSlider.getWidgetCount());
		backgroundSlider.selectTab(initialSlide);		
		setupHeaderPanel();
		LayoutPanel temp = (LayoutPanel) backgroundSlider.getWidget(initialSlide);
		temp.add(headerPanel);
		
		//tl.addStyleName("tabLayoutStyle");
		RootLayoutPanel.get().clear();
		RootLayoutPanel.get().add(backgroundSlider);
		startSlide();
	}
	
	private void startSlide()
	{
		playThread = new Timer() {
			boolean isVertical = true;
			
			@Override
			public void run() {
				int i = backgroundSlider.getSelectedIndex();
				i++;
				i = i % backgroundSlider.getWidgetCount();
				backgroundSlider.selectTab(i);
				addControlsToPanel(i);
				isVertical = !isVertical;
				backgroundSlider.setAnimationVertical(isVertical);
			}
		};
		playThread.scheduleRepeating(10000);
	}
	
	private void playSlide()
	{
		if(playThread == null)
			startSlide();
	}
	
	private void pauseSlide()
	{
		if(playThread != null)
		{
			playThread.cancel();
			playThread = null;
		}
	}
	
	private FlowPanel getMenuItems()
	{
		String baseUrl = "http://ww2.newprudential.com/";
		String[] labels = {"About Us", "Products", "Properties", "Mortgage Tips", "Careers", "Contact Us"};
		String[] href = {"about-us", "our-products", "properties", "mortgage-tips", "careers", "contact-us"};
		FlowPanel container = new FlowPanel();
        
		//setup HTMLPanels to Display
		final Widget[] displays = new Widget[labels.length];
		displays[0] = new AboutUs();
		displays[1] = new Products();
		displays[2] = new Properties();
		displays[3] = new Tips();
		displays[4] = new Careers();
		displays[5] = new ContactUs();
		
		//setup popup
        //hoverDisplay.setGlassEnabled(true);
        hoverDisplay.addStyleName(primaryBundle.prudential().popupStyle());
        hoverDisplay.setAnimationEnabled(true);
        hoverDisplay.setAutoHideEnabled(false);
        hoverDisplay.setAutoHideOnHistoryEventsEnabled(true);
        
        FlowPanel contentArea = new FlowPanel();
        final SimplePanel display = new SimplePanel();
        display.addStyleName(primaryBundle.prudential().popupButton());
        Image logo = new Image(primaryBundle.logoimage());
        logo.addStyleName(primaryBundle.prudential().bottomLogo());
        contentArea.add(display);
        contentArea.add(logo);
        FocusPanel hoverInOut = new FocusPanel();
        hoverInOut.add(contentArea);
        hoverDisplay.add(hoverInOut);
        final int hoverWidth = 390;
        final int hoverHeight = 320;
        hoverDisplay.setPixelSize(hoverWidth, hoverHeight);
        hoverInOut.addMouseOutHandler(new MouseOutHandler() {
			
			@Override
			public void onMouseOut(MouseOutEvent event) {
				int x = Math.abs(event.getRelativeX(hoverDisplay.getElement()));
				if(x > 5)
					hoverDisplay.hide();
			}
		});
        
		for(int i = 0; i < labels.length; i++)
		{
			final Anchor a = new Anchor(labels[i], baseUrl + href[i]);
			a.addStyleName(primaryBundle.prudential().simpleAnchor());
			container.add(a);
			final Widget w = displays[i];
			a.addMouseOverHandler(new MouseOverHandler() {
				
				@Override
				public void onMouseOver(MouseOverEvent event) {
					display.clear();
					display.add(w);
					//hoverDisplay.center();
					hoverDisplay.show();

				}
			});
			
			a.addMouseOutHandler(new MouseOutHandler() {
				int threshold = 40;
				//do we really want to do anything with this
				@Override
				public void onMouseOut(MouseOutEvent event) {
					int x = Math.abs(event.getRelativeX(hoverDisplay.getElement()));
					if(x > threshold)
						hoverDisplay.hide();
				}
			});
		}
		
		return container;
	}
	
	private void  moveSlide(boolean isForward)
	{
		backgroundSlider.setAnimationVertical(false);
		int i = backgroundSlider.getSelectedIndex();
		if(isForward)
		{
			i++;
			i = i % backgroundSlider.getWidgetCount();
		}
		else
		{
			i--;
			if(i < 0)
				i = backgroundSlider.getWidgetCount() - 1;
		}		
		backgroundSlider.selectTab(i);
		addControlsToPanel(i);
	}
	
	private void addControlsToPanel(int i)
	{
		headerPanel.removeFromParent();
		((LayoutPanel) backgroundSlider.getWidget(i)).add(headerPanel);		
	}
}
