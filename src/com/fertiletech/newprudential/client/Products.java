package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Products extends Composite{
	@UiField
	SimplePanel history;

	@UiField
	SimplePanel directors;
	
	@UiField
	SimplePanel management;
	
	@UiField
	SimplePanel compliance;
	private static ProductsUiBinder uiBinder = GWT
			.create(ProductsUiBinder.class);

	interface ProductsUiBinder extends UiBinder<Widget, Products> {
	}

	public Products() {
		initWidget(uiBinder.createAndBindUi(this));
		history.add(new Image(DisplayHelper.menuBundle.deposit()));
		management.add(new Image(DisplayHelper.menuBundle.lending()));
		directors.add(new Image(DisplayHelper.menuBundle.services()));
		compliance.add(new Image(DisplayHelper.menuBundle.download()));		
	}

}
