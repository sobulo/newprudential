package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Careers extends Composite {
	@UiField
	SimplePanel history;
	private static CareersUiBinder uiBinder = GWT.create(CareersUiBinder.class);

	interface CareersUiBinder extends UiBinder<Widget, Careers> {
	}

	public Careers() {
		initWidget(uiBinder.createAndBindUi(this));
		history.add(new Image(DisplayHelper.menuBundle.careers()));
	}

}
