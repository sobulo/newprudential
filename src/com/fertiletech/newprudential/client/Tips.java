package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Tips extends Composite {
	@UiField
	SimplePanel history;

	@UiField
	SimplePanel directors;
	
	@UiField
	SimplePanel management;
	
	private static TipsUiBinder uiBinder = GWT.create(TipsUiBinder.class);

	interface TipsUiBinder extends UiBinder<Widget, Tips> {
	}

	public Tips() {
		initWidget(uiBinder.createAndBindUi(this));
		history.add(new Image(DisplayHelper.menuBundle.mortgage()));
		management.add(new Image(DisplayHelper.menuBundle.choosehome()));
		directors.add(new Image(DisplayHelper.menuBundle.eligible()));
	}

}
