package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MaintenanceNotice extends Composite {

	private static MaintenanceNoticeUiBinder uiBinder = GWT
			.create(MaintenanceNoticeUiBinder.class);

	interface MaintenanceNoticeUiBinder extends
			UiBinder<Widget, MaintenanceNotice> {
	}

	public MaintenanceNotice() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
