package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ContactUs extends Composite {
	@UiField
	SimplePanel history;
	
	private static ContactUsUiBinder uiBinder = GWT
			.create(ContactUsUiBinder.class);

	interface ContactUsUiBinder extends UiBinder<Widget, ContactUs> {
	}

	public ContactUs() {
		initWidget(uiBinder.createAndBindUi(this));
		history.add(new Image(DisplayHelper.menuBundle.contact()));
	}

}
