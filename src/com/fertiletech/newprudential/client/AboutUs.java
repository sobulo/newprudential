package com.fertiletech.newprudential.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class AboutUs extends Composite{

	@UiField
	SimplePanel history;

	@UiField
	SimplePanel directors;
	
	@UiField
	SimplePanel management;
	
	@UiField
	SimplePanel compliance;
	
	private static AboutUsUiBinder uiBinder = GWT.create(AboutUsUiBinder.class);

	interface AboutUsUiBinder extends UiBinder<Widget, AboutUs> {
	}

	public AboutUs() {
		initWidget(uiBinder.createAndBindUi(this));
		final Image historyImage = new Image(DisplayHelper.menuBundle.history());
		history.add(historyImage);
		final Image managementImage = new Image(DisplayHelper.menuBundle.manage());
		management.add(managementImage);
		directors.add(new Image(DisplayHelper.menuBundle.director()));
		compliance.add(new Image(DisplayHelper.menuBundle.affil()));		
	}

}
